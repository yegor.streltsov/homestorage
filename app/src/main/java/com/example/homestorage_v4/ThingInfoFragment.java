package com.example.homestorage_v4;

import static com.example.homestorage_v4.MainActivity.connection;
import static com.example.homestorage_v4.MainActivity.dbConnection;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homestorage_v4.frames.SpeakToMicFrame;
import com.example.homestorage_v4.frames.ThingInfoFrame;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ThingInfoFragment extends Fragment {
    public static int lastInsertedId;
    private FrameLayout thingInfoFrameContainer;
    public static ThingInfoFrame thingInfoFrame;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_thing_info, null);

        thingInfoFrameContainer = view.findViewById(R.id.thingInfoContainer);
        thingInfoFrame = new com.example.homestorage_v4.frames.ThingInfoFrame(getContext());
        //thingInfoFrame.liId = lastInsertedId;
        thingInfoFrameContainer.addView(thingInfoFrame);


        return view;
    }


}