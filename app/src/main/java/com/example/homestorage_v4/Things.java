package com.example.homestorage_v4;

public class Things {
    private int id;
    private String name;
    public int getId(){return id;}
    public String getName(){return name;}
    public void setId(int _id){
        if (_id > -1) id = _id;
    }
    public void setName(String _name){
        if (_name.length() > 0) name = _name;
    }

}
