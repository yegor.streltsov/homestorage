package com.example.homestorage_v4;

import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class SearchResultsFragment extends Fragment {
    //ArrayList<Things> things_fc = new ArrayList<Things>();
    //ArrayList<String> things = new ArrayList<String>();
    //ArrayAdapter<String> adapter;
    //List<String> list;

    public static SearchResultsListAdapter searchResultsListAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<Integer>> listDataChild;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, null);
        getActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                MainActivity.changeToHomeFragment();
            }

        });

        expListView = view.findViewById(R.id.searchResultsExpandableListView);
        prepareOrUpdateListData();


        sendQueryToDb();

        return view;
    }

    public void prepareOrUpdateListData(){
        int grouId = -1;
        int roomsCount = 5; // TODO Заменить на количество записей возвращённых запросом
        int floorsCount = 5; // TODO то же что и выше

        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();
        List<Integer> childItemList;

        for (int roomIndex = 0; roomIndex < roomsCount; roomIndex++){
            String room_name = MainActivity.lightingsRoomNames[roomIndex];
            childItemList = new ArrayList<>();
            boolean isHeaderAdded = false;

            for (int floorIndex = 0; floorIndex < floorsCount; floorIndex++){
                if (MainActivity.floorsRoomsId[floorIndex] == roomIndex){
                    if (!isHeaderAdded){
                        listDataHeader.add(room_name);
                        isHeaderAdded = true;
                        grouId++;
                    }
                    childItemList.add(floorIndex);
                    listDataChild.put(listDataHeader.get(grouId), childItemList);
                }
            }
        }
        searchResultsListAdapter = new SearchResultsListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(searchResultsListAdapter);
    }

    public void sendQueryToDb() {

        String sql_query_things = "SELECT Things.ID, Things.NAME, Things.DATE_TIME, Things.CATEGORY_ID, " +
                "Container_types.cont_type, Containers.number, Real_user_names.REAL_USER_NAME " +
                "FROM Things JOIN Containers ON Things.container_id = Containers.ID " +
                "JOIN Container_types ON Containers.type_id = Container_types.ID " +
                "JOIN Real_user_names ON Things.REAL_USER_ID = Real_user_names.ID " +
                "WHERE Things.NAME LIKE '%" + MainActivity.searchString + "%'";

        try {
            MainActivity.statement = MainActivity.connection.createStatement();
            try (final ResultSet result = MainActivity.statement.executeQuery(sql_query_things)){
                adapter.clear();
                while (result.next()){
                    int id = result.getInt(1);
                    String name = result.getString(2);
                    String cont_type = result.getString(5);
                    int cont_num = result.getInt(6);

                    add(id, name, cont_type, cont_num);
                }
            } catch (Exception m){
                Toast.makeText(getActivity(), "inside exc", Toast.LENGTH_LONG).show();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public void add(int id, String name, String cont_type, int cont_num){
        if(!name.isEmpty()){
            Things currentThing = new Things();
            currentThing.setId(id);
            currentThing.setName(name);
            //things_fc.add(currentThing);
            adapter.add(name + " : " + cont_type + " #" + String.valueOf(cont_num));
            adapter.notifyDataSetChanged();
        }
    }
}