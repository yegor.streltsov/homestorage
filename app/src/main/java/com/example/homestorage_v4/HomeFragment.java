package com.example.homestorage_v4;

//import static com.example.homestorage_v4.MainActivity.homeFragment;

import static android.app.Activity.RESULT_OK;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.homestorage_v4.frames.AddFrame;
import com.example.homestorage_v4.frames.SearchFrame;
import com.example.homestorage_v4.frames.SettingsFrame;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

public class HomeFragment extends Fragment {
    ImageButton btnSearch;
    ImageButton btnAdd;
    private FrameLayout searchFrameContainer;
    private FrameLayout addFrameContainer;
    private FrameLayout settingsFrameContainer;
    public static SearchFrame searchFrame;
    public static AddFrame addFrame;
    public static SettingsFrame settingsFrame;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null);

        searchFrameContainer = view.findViewById(R.id.searchFrameContainer);
        addFrameContainer = view.findViewById(R.id.addFrameContainer);
        settingsFrameContainer = view.findViewById(R.id.settingsFrameContainer);
        searchFrame = new com.example.homestorage_v4.frames.SearchFrame(getContext());
        addFrame = new com.example.homestorage_v4.frames.AddFrame(getContext());
        settingsFrame = new com.example.homestorage_v4.frames.SettingsFrame(getContext());
        searchFrameContainer.addView(searchFrame);
        addFrameContainer.addView(addFrame);
        settingsFrameContainer.addView(settingsFrame);

        ActivityResultLauncher<Intent> startActivityIntent = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        // Add same code that you want to add in onActivityResult method
                        if (result != null && result.getResultCode() == RESULT_OK) {
                            if (result.getData() != null && result.getData().getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0) != null){
                                ArrayList<String> res = result.getData().getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                                //tv_Speech_to_text.setText(Objects.requireNonNull(res).get(0));
                                // Здесь будет код поиска в БД предмета "Objects.requireNonNull(res).get(0)"
                                MainActivity.searchString = Objects.requireNonNull(res).get(0);
                                MainActivity.changeToSearchResultsFragment();
                            }
                        }
                    }
                });

        searchFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Назовите искомый предмет");
                try {
                    startActivityIntent.launch(intent);
                }
                catch (Exception e) {
                    Toast.makeText(getActivity(), " " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                if (MainActivity.connection == null) {
                    MainActivity.dbConnection(v);
                }

                //MainActivity.changeToSearchResultsFragment();
            }
        });

        addFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.changeToAddThingFragment();
            }
        });

        return view;

    }
}