package com.example.homestorage_v4;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import java.util.HashMap;
import java.util.List;

public class SearchResultsListAdapter extends BaseExpandableListAdapter {
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Integer>> _listDataChild;

    public SearchResultsListAdapter(SearchResultsFragment context, List<String> listDataHeader, HashMap<String, List<Integer>> listChildData){
        this._context = context.getContext();
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public int getGroupCount(){
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // Было так: return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
        // Исправлено на нижеуказанный код для избежания ошибки при открытии пустой группы
        if (this._listDataChild.get(this._listDataHeader.get(groupPosition)) == null)
            return 0;
        else
            return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        final int childArrayId = (Integer) getChild(groupPosition, childPosition);
        String childText = MainActivity.floorNames[childArrayId];


        return  convertView;
    }

}
