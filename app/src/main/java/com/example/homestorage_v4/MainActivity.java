package com.example.homestorage_v4;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class MainActivity extends AppCompatActivity {
    final static String TAG_HOME_FRAGMENT = "TAG_HOME_FRAGMENT";
    final static String TAG_SEARCH_FRAGMENT = "TAG_SEARCH_FRAGMENT";
    final static String TAG_ADD_FRAGMENT = "TAG_ADD_FRAGMENT";
    final static String TAG_THINGINFO_FRAGMENT = "TAG_THINGINFO_FRAGMENT";
    final static public String KEY_MSG_HOME_FRAGMENT = "KEY_MSG_HOME_FRAGMENT";
    final static public String KEY_MSG_SEARCH_FRAGMENT = "KEY_MSG_SEARCH_FRAGMENT";
    final static public String KEY_MSG_ADD_FRAGMENT = "KEY_MSG_ADD_FRAGMENT";
    final static public String KEY_MSG_THINGINFO_FRAGMENT = "KEY_MSG_THINGINFO_FRAGMENT";
    public static String db_address = "78.36.11.47";
    public static int db_port = 3307;
    public static String db_name = "HomeStorage";
    public static String db_user = "testuser";
    public static String db_user_real_name = "Егор";
    public static int db_real_user_id = 1;
    public static String db_password = "EZS65@03bE";
    public static Connection connection = null;
    public static Statement statement = null;
    //public static Thing currentThing;
    public static FragmentManager fragmentManager;
    public static HomeFragment homeFragment;
    public static SearchResultsFragment searchResultsFragment;
    public static AddThingFragment addThingFragment;
    public static ThingInfoFragment thingInfoFragment;
    //public static FragmentTransaction fragmentTransaction;
    private FrameLayout frameLayout;
    public static int currentFragmentIndex = 0;
    /*
    homeFragment - 0
    searchResultsFragment - 1
    addThingFragment - 2
    thingInfoFragment - 3
    */

    public static String searchString;

    public static String [] floorNames = new String[/*floorsCount*/] {"Первый предмет", "Второй предмет", "Некая вещь", "Ещё какая-то фигня", "Хрень ненужная"};
    public static int[] floorsRoomsId = new int[/*floorsCount*/] {0, 1, 2, 3, 4};
    public static String[] lightingsRoomNames = new String[/*lightingsRoomsCount*/] {"Первый предмет", "Второй предмет", "Некая вещь", "Ещё какая-то фигня", "Хрень ненужная"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int SDK_INT = Build.VERSION.SDK_INT;
        if (SDK_INT > 8){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();

        homeFragment = new HomeFragment();
        searchResultsFragment = new SearchResultsFragment();
        addThingFragment = new AddThingFragment();
        thingInfoFragment = new ThingInfoFragment();

        changeToHomeFragment();
    }

    public static void  changeToHomeFragment(){
        currentFragmentIndex = 0;
        HomeFragment fragment = (HomeFragment) fragmentManager.findFragmentByTag(TAG_HOME_FRAGMENT);
        if (fragment == null){
            Bundle bundle = new Bundle();
            bundle.putString(KEY_MSG_HOME_FRAGMENT, null);
            homeFragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout, homeFragment, TAG_HOME_FRAGMENT);
            fragmentTransaction.commit();
        }
    }

    public static void  changeToSearchResultsFragment(){
        currentFragmentIndex = 1;
        SearchResultsFragment fragment = (SearchResultsFragment) fragmentManager.findFragmentByTag(TAG_SEARCH_FRAGMENT);
        if (fragment == null){
            Bundle bundle = new Bundle();
            bundle.putString(KEY_MSG_SEARCH_FRAGMENT, null);
            searchResultsFragment.setArguments(bundle);

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout, searchResultsFragment, TAG_SEARCH_FRAGMENT);
            fragmentTransaction.commit();
        }
    }

    public static void  changeToAddThingFragment(){
        currentFragmentIndex = 2;
        AddThingFragment fragment = (AddThingFragment) fragmentManager.findFragmentByTag(TAG_ADD_FRAGMENT);
        if (fragment == null){
            Bundle bundle = new Bundle();
            bundle.putString(KEY_MSG_ADD_FRAGMENT, null);
            addThingFragment.setArguments(bundle);

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout, addThingFragment, TAG_ADD_FRAGMENT);
            fragmentTransaction.commit();
        }
    }

    public static void  changeToThingInfoFragment(int _lastInsertedId){
        currentFragmentIndex = 3;
        ThingInfoFragment fragment = (ThingInfoFragment) fragmentManager.findFragmentByTag(TAG_THINGINFO_FRAGMENT);
        if (fragment == null){
            Bundle bundle = new Bundle();
            bundle.putString(KEY_MSG_THINGINFO_FRAGMENT, null);
            thingInfoFragment.setArguments(bundle);

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout, thingInfoFragment, TAG_THINGINFO_FRAGMENT);
            thingInfoFragment.lastInsertedId = _lastInsertedId;
            fragmentTransaction.commit();

        }
    }

    public static void dbConnection(View v) {
        final String url = "jdbc:mariadb://" + db_address + ":" + String.valueOf(db_port) +
                "/" + db_name;
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            connection = (Connection) DriverManager.getConnection(url, db_user, db_password);
            if (!connection.isClosed()) Toast.makeText(v.getContext(), R.string.db_connected, Toast.LENGTH_LONG).show();
            statement = (Statement) connection.createStatement();
            System.out.println("Database connection established");
        } catch (Exception e) {
            Toast.makeText(v.getContext(), R.string.db_not_connected, Toast.LENGTH_LONG).show();
            System.out.println(e.getMessage() + " ///////////////////////////////");
        }
    }

}