package com.example.homestorage_v4;

import static android.app.Activity.RESULT_OK;

import static com.example.homestorage_v4.MainActivity.changeToThingInfoFragment;
import static com.example.homestorage_v4.MainActivity.connection;
import static com.example.homestorage_v4.MainActivity.dbConnection;
import static com.example.homestorage_v4.MainActivity.db_real_user_id;
import static com.example.homestorage_v4.MainActivity.db_user_real_name;
import static com.example.homestorage_v4.MainActivity.statement;
import static com.example.homestorage_v4.R.id.edtAddingThingName;
import static com.example.homestorage_v4.R.id.tvContainerAndNum;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homestorage_v4.frames.AddFrame;
import com.example.homestorage_v4.frames.AddingThingNameFrame;
import com.example.homestorage_v4.frames.ContainerAndNumFrame;
import com.example.homestorage_v4.frames.SaveFrame;
import com.example.homestorage_v4.frames.ScanFrame;
import com.example.homestorage_v4.frames.SearchFrame;
import com.example.homestorage_v4.frames.SettingsFrame;
import com.example.homestorage_v4.frames.SpeakToMicFrame;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.sql.Statement;

public class AddThingFragment extends Fragment {
    private FrameLayout speakToMicFrameContainer;
    private FrameLayout addingThingNameFrameContainer;
    private FrameLayout scanFrameContainer;
    private FrameLayout containerAndNumFrameContainer;
    private FrameLayout saveFrameContainer;
    public static SpeakToMicFrame speakToMicFrame;
    public static AddingThingNameFrame addingThingNameFrame;
    public static ScanFrame scanFrame;
    public static ContainerAndNumFrame containerAndNumFrame;
    public static SaveFrame saveFrame;
    TextView tvContainer;
    //Button btnSaveThingToDb;
    int contType, contNumber, contId;
    public static String thingName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ImageButton btnNameTheThing;
        EditText edtThingName;
        ImageButton btnScanQr;

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_thing, null);

        speakToMicFrameContainer = view.findViewById(R.id.speakToMicFrameContainer);
        addingThingNameFrameContainer = view.findViewById(R.id.addingThingNameFrameContainer);
        scanFrameContainer = view.findViewById(R.id.scanFrameContainer);
        containerAndNumFrameContainer = view.findViewById(R.id.containerAndNumContainer);
        saveFrameContainer = view.findViewById(R.id.saveButtonContainer);
        speakToMicFrame = new com.example.homestorage_v4.frames.SpeakToMicFrame(getContext());
        addingThingNameFrame = new com.example.homestorage_v4.frames.AddingThingNameFrame(getContext());
        scanFrame = new com.example.homestorage_v4.frames.ScanFrame(getContext());
        containerAndNumFrame = new com.example.homestorage_v4.frames.ContainerAndNumFrame(getContext());
        saveFrame = new com.example.homestorage_v4.frames.SaveFrame(getContext());
        speakToMicFrameContainer.addView(speakToMicFrame);

        getActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                MainActivity.changeToHomeFragment();
            }

        });


        ActivityResultLauncher<Intent> startActivityIntent = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        // Add same code that you want to add in onActivityResult method
                        if (result != null && result.getResultCode() == RESULT_OK) {
                            if (result.getData() != null && result.getData().getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0) != null){
                                ArrayList<String> res = result.getData().getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                                //tv_Speech_to_text.setText(Objects.requireNonNull(res).get(0));
                                // Здесь будет код поиска в БД предмета "Objects.requireNonNull(res).get(0)"
                                addingThingNameFrame.edtAddingThingName.setText(Objects.requireNonNull(res).get(0));
                                thingName = Objects.requireNonNull(res).get(0);
                                addingThingNameFrameContainer.addView(addingThingNameFrame);
                                scanFrameContainer.addView(scanFrame);
                            }
                        }
                    }
                });

        speakToMicFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Произнесите название предмета");
                try {
                    startActivityIntent.launch(intent);
                }
                catch (Exception e) {
                    Toast.makeText(getActivity(), " " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        scanFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Запуск сканирования QR кода
                ScanOptions scanOptions = new ScanOptions();
                barcodeLauncher.launch(scanOptions);
            }
        });

        saveFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContainerIdByNumber();
                String sql_insert_thing = "INSERT INTO HomeStorage.Things(NAME, container_id, REAL_USER_ID) VALUES (?, ?, ?)";
                try (PreparedStatement statement = MainActivity.connection.prepareStatement(sql_insert_thing)) {
                    statement.setString(1, thingName);
                    statement.setInt(2, contId);
                    statement.setInt(3, db_real_user_id);
                    int rowsInserted = statement.executeUpdate();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                changeToThingInfoFragment(getLastInsertId());
            }
        });

        return view;
    }

    private final ActivityResultLauncher<ScanOptions> barcodeLauncher = registerForActivityResult(
            new ScanContract(),
            result -> {
                if (result.getContents() != null){
                    String constString = result.getContents();
                    if (containerNameUpdate(constString)){
                        containerAndNumFrameContainer.addView(containerAndNumFrame);
                    }
                } else {
                    Toast.makeText(getActivity(),
                            "Ошибка сканирования QR кода. Попробуйте ещё раз.",
                            Toast.LENGTH_LONG).show();
                }
            }
    );

    private boolean containerNameUpdate(String constString){
        boolean updResult = false;
        // TYPE=0 NUMBER=4
        String sContType = "";
        String sContNumber = "";
        try {
            sContType = constString.substring(constString.indexOf("TYPE=") + 5, constString.indexOf(" NUMBER="));
            contType = Integer.valueOf(sContType);
            sContNumber = constString.substring(constString.indexOf(" NUMBER=") + 8);
            contNumber = Integer.valueOf(sContNumber);
            // При появлении дополнительных типов контейнеров код ниже нужно допилить
            if (contType == 0 && contNumber > 0 && contNumber < 6){
                com.example.homestorage_v4.frames.ContainerAndNumFrame.tvContainerAndNum.setText("Коробка #" + contNumber);
                //TODO Необходимо устранить ошибку, которая возникает в этом блоке кода и вызывает исключение
                saveFrameContainer.addView(saveFrame);
                updResult = true;
            } else {
                com.example.homestorage_v4.frames.ContainerAndNumFrame.tvContainerAndNum.setText(sContType + "|" +sContNumber);
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Неверный QR-код. Возможно вы отсканировали другой QR-код. Попробуйте ещё раз.", Toast.LENGTH_LONG).show();
        }
        return updResult;
    }

    private void getContainerIdByNumber(){
        if (connection == null) dbConnection(getView());
        String sql_get_id_query = "SELECT ID FROM HomeStorage.Containers WHERE number=" + String.valueOf(contNumber);
        try {
            MainActivity.statement = MainActivity.connection.createStatement();
            try (final ResultSet result = MainActivity.statement.executeQuery(sql_get_id_query)){

                while (result.next()){
                    contId = result.getInt(1);
                }
            } catch (Exception m){
                Toast.makeText(getActivity(), "wrong id exc", Toast.LENGTH_LONG).show();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private int getLastInsertId(){
        int lastInsertId = -1;
        if (connection == null) dbConnection(getView());
        String sql_get_id_query = "SELECT LAST_INSERT_ID();";
        try {
            MainActivity.statement = MainActivity.connection.createStatement();
            try (final ResultSet result = MainActivity.statement.executeQuery(sql_get_id_query)){
                while (result.next()){
                    lastInsertId = result.getInt(1);
                }
            } catch (Exception m){
                Toast.makeText(getActivity(), "wrong id exc", Toast.LENGTH_LONG).show();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lastInsertId;
    }

}