package com.example.homestorage_v4.frames;

import static com.example.homestorage_v4.MainActivity.connection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homestorage_v4.MainActivity;


import androidx.constraintlayout.widget.ConstraintLayout;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SearchFrame extends ConstraintLayout {
    public TextView tvThingsTotalCount;
    public TextView tvThingsCountText;
    public SearchFrame(Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View frameView = inflater.inflate(com.example.homestorage_v4.R.layout.search_frame, this);
        tvThingsTotalCount = frameView.findViewById(com.example.homestorage_v4.R.id.tvTotalThingsCount);
        tvThingsCountText = frameView.findViewById(com.example.homestorage_v4.R.id.tvThingName);
        // Далее инизциализируем компоненты
        initComponent(frameView);
    }
    private void initComponent(View view) {

        if (connection == null) MainActivity.dbConnection(view);

        String sql_query_things = "SELECT COUNT(*) FROM Things";

        try {
            MainActivity.statement = connection.createStatement();
            try (final ResultSet result = MainActivity.statement.executeQuery(sql_query_things)){
                while (result.next()){
                    int count = result.getInt(1);
                    tvThingsTotalCount.setText(String.valueOf(count));
                    setTvThingsCountText(count);
                }

            } catch (Exception m){
                Toast.makeText(getContext(), "inside exc", Toast.LENGTH_LONG).show();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void setTvThingsCountText(int thingsCount){
        boolean moreThan9lessThan15 = false;
        if ((thingsCount > 9 && thingsCount < 15) || (thingsCount%100 > 9 && thingsCount%100 < 15)) moreThan9lessThan15 = true;

        int lastNum = thingsCount % 10;
        if (lastNum == 1 && !moreThan9lessThan15){
            tvThingsCountText.setText(getResources().getString(com.example.homestorage_v4.R.string.things_in_storage_now_0)); // вещь
            return;
        }
        if (lastNum > 1 && lastNum < 5 && !moreThan9lessThan15){
            tvThingsCountText.setText(getResources().getString(com.example.homestorage_v4.R.string.things_in_storage_now_1)); // вещи
            return;
        }

        tvThingsCountText.setText(getResources().getString(com.example.homestorage_v4.R.string.things_in_storage_now_2)); // вещей
    }
}
