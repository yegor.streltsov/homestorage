package com.example.homestorage_v4.frames;

import static com.example.homestorage_v4.MainActivity.thingInfoFragment;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.homestorage_v4.AddThingFragment;
import com.example.homestorage_v4.MainActivity;
import com.example.homestorage_v4.R;
import com.example.homestorage_v4.ThingInfoFragment;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ThingInfoFrame extends ConstraintLayout {
    private TextView tvThingName;
    private TextView tvContainer;
    private TextView tvUser;
    private TextView tvDateTime;
    public int liId;
    public ThingInfoFrame(Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View frameView = inflater.inflate(R.layout.thing_info_frame, this);
        liId = thingInfoFragment.lastInsertedId;
        // Далее инизциализируем компоненты

        initComponent(frameView);
    }
    private void initComponent(View view) {
        tvThingName = findViewById(R.id.tvThingName);
        tvContainer = findViewById(R.id.tvContainerName);
        tvUser = findViewById(R.id.tvRealUserName);
        tvDateTime = findViewById(R.id.tvDateTime);
        //updateFields();
        String sql_query_thing_info = "SELECT Things.NAME, Things.DATE_TIME, Things.CATEGORY_ID, " +
                "Container_types.cont_type, Containers.number, Real_user_names.REAL_USER_NAME " +
                "FROM Things JOIN Containers ON Things.container_id = Containers.ID " +
                "JOIN Container_types ON Containers.type_id = Container_types.ID " +
                "JOIN Real_user_names ON Things.REAL_USER_ID = Real_user_names.ID " +
                "WHERE Things.ID = " + String.valueOf(liId) + ";";

        try {
            MainActivity.statement = MainActivity.connection.createStatement();
            try (final ResultSet result = MainActivity.statement.executeQuery(sql_query_thing_info)){
                while (result.next()){
                    String name = result.getString(1);
                    String dateTime = result.getString(2);
                    String contType = result.getString(4);
                    int contNum = result.getInt(5);
                    String user = result.getString(6);

                    updateFields(name, contType, contNum, dateTime ,user);
                }
            } catch (Exception m){
                Toast.makeText(getContext(), "inside exc", Toast.LENGTH_LONG).show();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void updateFields(String _name, String _contType, int _contNum, String _dateTime, String _user){
        tvThingName.setText(_name);
        tvContainer.setText(_contType + " #" + String.valueOf(_contNum));
        tvDateTime.setText(_dateTime);
        tvUser.setText(_user);
    }




}
