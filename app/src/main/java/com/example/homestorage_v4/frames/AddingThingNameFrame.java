package com.example.homestorage_v4.frames;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.homestorage_v4.AddThingFragment;
import com.example.homestorage_v4.R;

public class AddingThingNameFrame extends ConstraintLayout {
    public EditText edtAddingThingName;
    public AddingThingNameFrame(Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View frameView = inflater.inflate(R.layout.adding_thing_name_frame, this);
        // Далее инизциализируем компоненты
        edtAddingThingName = frameView.findViewById(R.id.edtAddingThingName);

        edtAddingThingName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                AddThingFragment.thingName = edtAddingThingName.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //initComponent();
    }
    private void initComponent() {


        //tvActiveLightingsCount = findViewById(R.id.tvActiveLightingsCount);
        //tvLightingsCount = findViewById(R.id.tvLightingLightingsCount);
        //ivFlash = findViewById(R.id.ivLightingLightingsFlash);
        //tvActivePower = findViewById(R.id.tvLightingLightingsPower);
        //tvLightingState = findViewById(R.id.tvVkl);

        //updateFields();
    }
}
